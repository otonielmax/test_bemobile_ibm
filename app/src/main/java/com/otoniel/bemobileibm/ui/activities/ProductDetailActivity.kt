package com.otoniel.bemobileibm.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.otoniel.bemobileibm.adapters.ProductsAdapter
import com.otoniel.bemobileibm.adapters.TransactionsAdapter
import com.otoniel.bemobileibm.databinding.ActivityProductDetailBinding
import com.otoniel.bemobileibm.model.Product
import com.otoniel.bemobileibm.utils.Utility.getRoundHalfEven

class ProductDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProductDetailBinding

    private lateinit var product: Product

    private lateinit var adapter: TransactionsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProductDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle = intent.extras
        if (bundle != null) {
            product = Gson().fromJson(bundle.getString("DATA"), Product::class.java)
        }
    }

    override fun onStart() {
        super.onStart()

        loadData()
    }

    private fun initRecyclerView() {
        adapter = TransactionsAdapter(product.transactions)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter
    }

    private fun loadData() {
        if (product != null) {
            binding.productSKU.text = product.sku
            binding.productSales.text = getRoundHalfEven(product.total).toString()

            initRecyclerView()
        }
    }
}