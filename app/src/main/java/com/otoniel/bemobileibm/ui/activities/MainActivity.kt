package com.otoniel.bemobileibm.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.otoniel.bemobileibm.R
import com.otoniel.bemobileibm.adapters.ProductsAdapter
import com.otoniel.bemobileibm.databinding.ActivityMainBinding
import com.otoniel.bemobileibm.databinding.ItemProductBinding
import com.otoniel.bemobileibm.model.HistoryCurrencyRoadMap
import com.otoniel.bemobileibm.model.Product
import com.otoniel.bemobileibm.network.APIService
import com.otoniel.bemobileibm.network.RateConversionResponse
import com.otoniel.bemobileibm.network.TransactionsResponse
import com.otoniel.bemobileibm.utils.Utility
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception
import java.math.BigDecimal
import java.util.*

class MainActivity : AppCompatActivity() {

    companion object {
        const val TAG = "MainActivity"
    }

    private var rateConversionOriginalList: MutableList<RateConversionResponse> = mutableListOf()
    private var rateConversionFromList: MutableList<RateConversionResponse> = mutableListOf()
    private var rateConversionRoadMapList: MutableList<RateConversionResponse> = mutableListOf()
    private var historyCurrencyRoadMap: MutableList<HistoryCurrencyRoadMap> = mutableListOf()
    private var transactionsList: MutableList<TransactionsResponse> = mutableListOf()
    private var productList: MutableList<Product> = mutableListOf()

    private var amountResult: BigDecimal = BigDecimal(0.0)

    private lateinit var adapter: ProductsAdapter

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initRecyclerView()
    }

    override fun onResume() {
        super.onResume()

        // Update rate conversion data
        // getRateConversionData()
    }

    override fun onStart() {
        super.onStart()

        if (rateConversionOriginalList.isEmpty()) {
            getRateConversionData()
        }
        if (transactionsList.isEmpty()) {
            getTransactionsData()
        }
    }

    private fun initRecyclerView() {
        adapter = ProductsAdapter(applicationContext, productList)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter
    }

    private fun getRateConversionData() {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = getRetrofit().create(APIService::class.java)
                    .getRateConversion("rates.json")
                val data = response.body()
                if (response.isSuccessful) {
                    Log.e(TAG, Gson().toJson(data))
                    rateConversionOriginalList = data as MutableList<RateConversionResponse>

                    // searchConversionCurrency("EUR", "AUD")
                } else {
                    showToast("Ocurrio un problema refrescando los datos")
                }
            } catch (e: Exception) {
                Log.e(TAG, "Exception $e")
                showToast("Ocurrio un error inesperado => $e")
            }

        }
    }

    private fun getTransactionsData() {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = getRetrofit().create(APIService::class.java)
                    .getTransactions("transactions.json")
                val data = response.body()
                if (response.isSuccessful) {
                    Log.e(TAG, Gson().toJson(data))
                    transactionsList = data as MutableList<TransactionsResponse>

                    groupTransactionByProduct()
                } else {
                    showToast("Ocurrio un problema refrescando los datos")
                }
            } catch (e: Exception) {
                Log.e(TAG, "Exception $e")
                showToast("Ocurrio un error inesperado => $e")
            }

        }
    }

    private fun searchConversionCurrency(from: String, to: String, amount: BigDecimal) {
        rateConversionRoadMapList = mutableListOf()

        for (item in rateConversionOriginalList) {
            if (item.from == from && item.to == to) {
                rateConversionRoadMapList.add(item)
                break
            }
        }

        // Si hay resultados con las monedas procedemos al calculo
        if (rateConversionRoadMapList.size > 0) {
            calculateCurrency(amount)
        } else {
            // Sino hacemos una busqueada exhaustiva en el listado principal
            searchForcedCurrencyConversion(from, to, amount)
        }
    }

    private fun searchForcedCurrencyConversion(
        from: String,
        to: String,
        amountConversion: BigDecimal
    ) {

        rateConversionFromList = filterRateByCurrency(from)

        // Si existe alguna coincidencia
        if (rateConversionFromList.size > 0) {
            val fromCurrency = rateConversionFromList[0]

            searchAdvancedCurrency(fromCurrency, to, amountConversion)
        } else {
            showToast("No se consiguio la moneda a convertir")
        }
    }

    private fun searchAdvancedCurrency(
        fromCurrency: RateConversionResponse,
        to: String,
        amountConversion: BigDecimal
    ) {

        var currency = fromCurrency

        var result: RateConversionResponse? = null

        while (result == null) {
            // rateConversionRoadMapList.add(fromCurrency)
            addNewRoadMap(fromCurrency)

            result = getCurrencyToEquals(currency, to)

            if (result == null) {
                val list = filterRateByCurrency(currency.to)
                if (list.size > 0) {
                    currency = list[0]
                }
            }
        }

        if (rateConversionRoadMapList.size > 0) {
            calculateCurrency(amountConversion)
        } else {
            showToast("No se consiguio una conversion para la busqueda")
        }

    }

    private fun filterRateByCurrency(from: String) : MutableList<RateConversionResponse> {
        val list: MutableList<RateConversionResponse> = mutableListOf()
        // Buscamos las conversiones que coinciden con la moneda de origen
        rateConversionOriginalList.forEach { it ->
            if (it.from == from) {
                list.add(it)
            }
        }
        return list
    }

    private fun getCurrencyToEquals(currency: RateConversionResponse, to: String) :
            RateConversionResponse? {
        Log.e(TAG, "getCurrencyToEquals ${currency.from}/${currency.to} => $to")
        var result : RateConversionResponse? = null

        for (item in rateConversionOriginalList) {
            Log.e(TAG, "Currency from ${currency.from}/${currency.to} " +
                    " to ${item.from}/${item.to}")
            if (currency.to == item.from && item.to == to) {
                Log.e(TAG, "Se consiguio la conversion")
                // rateConversionRoadMapList.add(currency)
                // rateConversionRoadMapList.add(item)
                addNewRoadMap(currency)
                addNewRoadMap(item)
                result = item
                break
            }
        }

        return result
    }

    private fun calculateCurrency(amountConversion: BigDecimal) {
        var amount = amountConversion

        rateConversionRoadMapList.forEach { it ->
            amount *= BigDecimal(it.rate)
        }

        val existIndexRoadMap = existRoadMapConversion(
            rateConversionRoadMapList.first().from,
            rateConversionRoadMapList.last().to
        )

        if (existIndexRoadMap == -1) {
            historyCurrencyRoadMap.add(
                HistoryCurrencyRoadMap(
                    rateConversionRoadMapList.first().from,
                    rateConversionRoadMapList.last().to,
                    rateConversionRoadMapList
                )
            )
        }

        Log.e(TAG, "Amount new $amount")
        Log.e(TAG, "Road Map " + Gson().toJson(rateConversionRoadMapList))

        amountResult = amount
    }

    private fun addNewRoadMap(currency: RateConversionResponse) {
        if (!rateConversionRoadMapList.contains(currency)) {
            rateConversionRoadMapList.add(currency)
        }
    }

    private fun groupTransactionByProduct() {
        productList = mutableListOf()
        if (transactionsList.isNotEmpty()) {
            for (tx in transactionsList) {
                var exist = -1
                for ((index, product) in productList.withIndex()) {
                    if (tx.sku == product.sku) {
                        exist = index
                        break
                    }
                }

                var amountToConverter = BigDecimal(tx.amount)
                if (tx.currency != "EUR") {
                    val roadMapIndex = existRoadMapConversion(tx.currency, "EUR")
                    if (roadMapIndex != -1) {
                        rateConversionRoadMapList = historyCurrencyRoadMap[roadMapIndex].roadMap
                        calculateCurrency(amountToConverter)
                    } else {
                        searchConversionCurrency(tx.currency, "EUR", amountToConverter)
                    }
                    amountToConverter = amountResult
                }

                tx.amountCoverted = amountToConverter

                if (exist == -1) {
                    val transactionsNew: MutableList<TransactionsResponse> = mutableListOf(tx)
                    productList.add(Product(tx.sku, amountToConverter, transactionsNew))
                } else {
                    productList[exist].transactions.add(tx)
                    productList[exist].total += amountToConverter
                }
            }

            Log.e(TAG, "Products " + Gson().toJson(productList))
        }

        this@MainActivity.runOnUiThread(Runnable {
            adapter.updateData(productList)
            adapter.notifyDataSetChanged()
        })
    }

    private fun existRoadMapConversion(from: String, to: String) : Int {
        var roadMapIndex = -1
        for ((index, item) in historyCurrencyRoadMap.withIndex()) {
            if (item.from == from && item.to == to) {
                roadMapIndex = index
                break
            }
        }
        return roadMapIndex
    }

    private fun showToast(message: String) {
        this@MainActivity.runOnUiThread(Runnable {
            Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        })
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Utility.URL_BASE)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}