package com.otoniel.bemobileibm.model

import com.otoniel.bemobileibm.network.RateConversionResponse

class HistoryCurrencyRoadMap(
    var from: String,
    var to: String,
    var roadMap: MutableList<RateConversionResponse>
) {
}