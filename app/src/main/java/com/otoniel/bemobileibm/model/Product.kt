package com.otoniel.bemobileibm.model

import com.otoniel.bemobileibm.network.TransactionsResponse
import java.math.BigDecimal

class Product(
    var sku: String,
    var total: BigDecimal,
    var transactions: MutableList<TransactionsResponse>
) {
}