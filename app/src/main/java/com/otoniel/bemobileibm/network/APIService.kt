package com.otoniel.bemobileibm.network

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Url

interface APIService {

    @Headers("Accept: application/json")
    @GET
    suspend fun getRateConversion(@Url endpoint:String): Response<List<RateConversionResponse>>

    @Headers("Accept: application/json")
    @GET
    suspend fun getTransactions(@Url endpoint:String): Response<List<TransactionsResponse>>
}