package com.otoniel.bemobileibm.network

import java.math.BigDecimal

data class TransactionsResponse(
    var sku: String,
    var amount: String,
    var currency: String,
    var amountCoverted: BigDecimal
)