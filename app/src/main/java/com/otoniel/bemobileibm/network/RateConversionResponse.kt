package com.otoniel.bemobileibm.network

data class RateConversionResponse(
    var from: String,
    var to: String,
    var rate: String
)