package com.otoniel.bemobileibm.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.otoniel.bemobileibm.R
import com.otoniel.bemobileibm.holders.ProductHolder
import com.otoniel.bemobileibm.model.Product
import com.otoniel.bemobileibm.ui.activities.ProductDetailActivity
import com.otoniel.bemobileibm.utils.Utility.getRoundHalfEven
import java.math.BigDecimal

class ProductsAdapter(
    private var context: Context,
    private var items: MutableList<Product>
    ) : RecyclerView.Adapter<ProductHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ProductHolder(layoutInflater.inflate(R.layout.item_product, parent, false))
    }
    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        val item: Product = items[position]

        val total = getRoundHalfEven(item.total)

        holder.bind(item.sku, "$total EUR")

        holder.binding.card.setOnClickListener {
            val intent = Intent(context, ProductDetailActivity::class.java)
            intent.putExtra("DATA", Gson().toJson(item))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent)
        }
    }

    fun updateData(items: MutableList<Product>) {
        this.items.clear()
        this.items = items
    }
}