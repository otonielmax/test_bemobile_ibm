package com.otoniel.bemobileibm.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.otoniel.bemobileibm.R
import com.otoniel.bemobileibm.holders.TransactionsHolder
import com.otoniel.bemobileibm.network.TransactionsResponse

class TransactionsAdapter(
    private var items: MutableList<TransactionsResponse>
) : RecyclerView.Adapter<TransactionsHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionsHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return TransactionsHolder(layoutInflater.inflate(R.layout.item_transaction, parent, false))
    }
    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: TransactionsHolder, position: Int) {
        val item: TransactionsResponse = items[position]

        holder.bind(item)
    }

    fun updateData(items: MutableList<TransactionsResponse>) {
        this.items.clear()
        this.items = items
    }
}