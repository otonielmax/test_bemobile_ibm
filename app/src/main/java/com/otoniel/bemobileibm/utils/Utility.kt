package com.otoniel.bemobileibm.utils

import java.math.BigDecimal

object Utility {
    const val URL_BASE : String = "http://quiet-stone-2094.herokuapp.com/"

    fun getRoundHalfEven(amount: BigDecimal) : BigDecimal {
        return amount.setScale(2, BigDecimal.ROUND_HALF_EVEN)
    }

    fun getTaxApplicated(amountResult: BigDecimal, amountOrigin: BigDecimal) : BigDecimal {
        return amountResult.divide(amountOrigin).setScale(2, BigDecimal.ROUND_HALF_EVEN)
    }
}