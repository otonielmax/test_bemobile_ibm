package com.otoniel.bemobileibm.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.otoniel.bemobileibm.databinding.ItemProductBinding

class ProductHolder(view: View): RecyclerView.ViewHolder(view) {

    val binding = ItemProductBinding.bind(view)

    fun bind(
        sku: String,
        amount: String
    ) {
        binding.productSKU.text = sku
        binding.productSales.text = amount
    }
}