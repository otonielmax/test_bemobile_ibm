package com.otoniel.bemobileibm.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.otoniel.bemobileibm.databinding.ItemTransactionBinding
import com.otoniel.bemobileibm.network.TransactionsResponse
import com.otoniel.bemobileibm.utils.Utility.getRoundHalfEven
import com.otoniel.bemobileibm.utils.Utility.getTaxApplicated
import java.math.BigDecimal

class TransactionsHolder(view: View): RecyclerView.ViewHolder(view) {

    val binding = ItemTransactionBinding.bind(view)

    fun bind(
        transactionsResponse: TransactionsResponse
    ) {
        binding.txTax.text = getTaxApplicated(
            transactionsResponse.amountCoverted,
            BigDecimal(transactionsResponse.amount)
        ).toString() + " %"
        binding.txCurrency.text = transactionsResponse.currency
        binding.txAmountOrigin.text =
            getRoundHalfEven(BigDecimal(transactionsResponse.amount)).toString() +
                    " ${transactionsResponse.currency}"
        binding.txAmountResult.text =
            getRoundHalfEven(transactionsResponse.amountCoverted).toString() + " EUR"
    }
}